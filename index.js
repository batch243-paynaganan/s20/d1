// console.log("Last session of the week!");

// [Section] While Loop

// while loop takes in an expression/condition.
// expressions are any unit of code that can be evaluated to a value.
// if the condition evaluates to be true the statements inside the code block will be executed.
// a loops will iterate a certain number of times until an expression
// iteration is the term given to the repitition of statements
/*
	Syntax:
	while(expression/condition){
	statements;
	iteration
	}
*/

/*let count =5;
// while the value of count is not equal to zero
while(count!==0){
	console.log("While: " + count);

	// decrement
	count--;
	console.log("Value of count after the iteration: " + count);
	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0;
	// loops occupy a significant amount of space in our devices.
	// make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop.
	// Forgetting to include this in loops will make our applications fun an infinite loop.
	// after running the script, if a slow response from the browser has experienced or an infinite loop is seen in the console quickly close the application/tab/browser to avoid this.
}*/

// [Section] Do-While Loop
/*
	-Do-while loop works a lot like the while loop but unlike while loops, do-while loops guarantee that the code will be executed at least once

	Syntax:
	do{
		statement;
		iteration;
	}
	while(expression/condition);
*/

/*let number = Number(prompt("Give me a number. "));

do{
	console.log("Do while: " + number);

	number++;
}
while(number<10)*/


// [Section] For loop
/*
	-For loop is more flexible than while and do while
	It consists of 3 parts:
	1. the "initialization" value that will track the progression of the loop
	2. the "expression" that will be evaluated which will determine whether the loop will run one more time.
	3. The "finalExpression" indicates how to advance the loop

	Syntax:
	for(initialization; expression/condition; finalExpression){
		statement/statements
	}
*/

	/*
		-we will create a loop that will start from 0 and end at 20
		-every iteratin of the loop, the value of count will be checked if it is less than or equal to 20
		-if the value of count is less than or equal to 20 the statement inside of the loop will execute.
		-the value of count will be incremented by one for each iteration
	*/

	/*for(let count=0; count<=20; count++){
		console.log("The current value of count is " + count) 
	}*/

// let myString = "alex";
	// characters in strings may be count using .length property
	// strings are special compare to other data types, that it has access to functions and other pieces of information

	// console.log(myString.length);

	// accessing character of a string
	// individual characters of a string may be accessed using it's index.
	// the first character in a string corresponds to 0, the next is 1, so on and so forth.

	/*console.log(myString[0]);
	console.log(myString[1]);
	console.log(myString[2]);
	console.log(myString[3]);*/

	// for(let i=0; i<myString.length; i++){
	// 	console.log(myString[i]);
	// }

	// let numA = 15;

	// for(let i=0; i<=10; i++){
	// 	let exponential = numA**i

	// 	console.log(exponential);
	// }


	// create a string named "myName" with value of "Alex"
	let myName = "Alex";

	/*
		create a loop that will print our the letters of the name individually and print our the number 3 instead when the letter to be printed is vowel
	*/

	for(let i=0; i<myName.length; i++){
		if(myName[i].toLowerCase() === "a" ||
			myName[i].toLowerCase() === "e" ||
			myName[i].toLowerCase() === "i" ||
			myName[i].toLowerCase() === "o" ||
			myName[i].toLowerCase() === "u"){
				console.log(3);
				}

		else{
			console.log(myName[i])
		}
	}

// [Section] Continue and break statements
/*
	the continue statements allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.
	-the break statement is used to terminate the current loop once a match has been found
*/

// creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop

// how this for loop works:
	/*
		1. the loop will start at 0 for the value of "count"
		2. it will check if count is less than the or equal to 20
		3. the if statement will check if the remainder of the value of count divided by 2 is equal to 0
		4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
        5. If the value of count is not equal to 0, the console will print the value of "count".
        6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "count" will be incremented by 1 (e.g. count = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement
	*/

	// for(let count=0; count<=20; i++){

	// }

	for (let count=0;count<=20;count++){
		// if remainder is equal to 0
			if (count%2===0){
				// tells the code to continue to the next iteration of the loop
				// this ignnores all statements located after the continue statement
				continue;
			}
			else if(count>10){
				// tells the code to terminate/stop the loop even if the expression/condition of the loops defines that it shoul execute so long as the value of count is less than or equal 
				break;
			}
		console.log("continue and break" + count)
	}


	let name="alexandro";
	for(let i=0; i<name.length; i++){
		console.log(name[i])
		if(name[i].toLowerCase() === "a"){
			console.log("continue to next iteration");
			continue;
		}

		if(name[i].toLowerCase()==="d"){
			console.log("console log before the break!");
			break;
		}
	}